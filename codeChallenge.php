<?php
require 'vendor/autoload.php';
require_once('httpClient.php');

Class codeChallenge
{
    // Name and Email
    const NAME = 'Chao Gu';
    const EMAIL = 'guchaocharlie@gmail.com';

    // endpoints
    const APPLY = "/apply";
    const GUESS = "/guess";
    const SUBMIT = "/submit_pack";

    private $stepMethod = [
        1 => 'doStepOne',
        2 => 'doStepTwo',
        3 => 'doStepThree',
        4 => 'doStepFour',
    ];

    // The url prefix for restful call
    private $urlPrefix = "https://game.eevi.life";

    // Current step being run
    private $currentStep;

    // The http client
    private $client;

    // ApplicationId and GameId being returned
    private $applicantID;
    private $gameID;

    private $packUrl = "https://gitlab.com/guchaosean/codechallenge";
    private $packInstructions = "See gitlab README.";

    public function __construct()
    {
        $this->currentStep = 1;
        $this->client = new httpClient();
    }

    private function getHalf($min, $max)
    {
        return round(($min + $max) / 2);
    }

    public function doStepOne()
    {
        $body = [
            'PreferredName' => self::NAME,
            'EmailAddress' => self::EMAIL,
        ];

        $response = $this->client->request('POST', $this->urlPrefix . self::APPLY, $body);
        
        $this->applicantID = json_decode($response)->ApplicantID;

        echo "Step one finish, I get the application id:" . $this->applicantID . "\n";

        $this->currentStep = 2;
    }

    public function doStepTwo()
    {
        $body = [
            'ApplicantID' => $this->applicantID,
        ];

        $response = $this->client->request('POST', $this->urlPrefix . self::GUESS, $body);

        $this->gameID = json_decode($response)->GameID;

        echo "Step two finish, I get the game id:" . $this->gameID . "\n";

        $this->currentStep = 3;
    }

    // Simple Binary Search
    public function doStepThree()
    {
        $min = 0;
        $max = 1000000;
        $won = false;

        while (!$won) {
            $half = $this->getHalf($min, $max);
            echo "I am guessing now, this time I guess: " . $half;
            $body = [
                'ApplicantID' => $this->applicantID,
                'GameID' => $this->gameID,
                'Guess' => $half,
            ];

            $response = $this->client->request('PUT', $this->urlPrefix . self::GUESS, $body);

            $won = json_decode($response)->Won;
            $result = json_decode($response)->HigherLower;
            $trick = json_decode($response)->DastardlyTrick;

            if (!$won) {
                echo ", unfortunately, this need to be " . $result . "\n";
            }

            if ($result === 'Lower') {
                $max = $half;
            }

            if ($result === 'Higher') {
                $min = $half;
            }

            // If tricky, reset min and max
            if ($trick) {
                $min = 0;
                $max = 1000000;
            }
        }

        if ($won) {
            $this->currentStep = 4;
        }
    }

    public function doStepFour()
    {

        $body = [
            'ApplicantID' => $this->applicantID,
            'GameID' => $this->gameID,
            'PackURL' => $this->packUrl,
            'PackInstructions' => $this->packInstructions,
        ];
        $response = $this->client->request('POST', $this->urlPrefix . self::SUBMIT, $body);

        echo "Final step, I am submitting the pack." . "\n";

        echo $response;

        $this->currentStep = 5;
    }


    public function move()
    {
        switch ($this->currentStep) {
            case 1:
                $this->doStepOne();
                break;
            case 2:
                $this->doStepTwo();
                break;
            case 3:
                $this->doStepThree();
                break;
            case 4:
                $this->doStepFour();
                break;
            default:
                $this->doStepOne();
                break;
        }
    }

    public function newMove()
    {
        $this->stepMethod[$this->currentStep]();
    }

    public function run()
    {
        while ($this->currentStep !== 5) {
            $this->move();
        }
    }
}

$codeChallenge = new codeChallenge();
$codeChallenge->run();