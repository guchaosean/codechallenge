<?php
require 'vendor/autoload.php';

Class httpClient
{
    // Client
    private $client;

    // Auth Username
    private $authUsername = "eevi applicant";

    // Auth Password
    private $authPassword = "love to code";

    public function __construct()
    {
        $this->client = new \GuzzleHttp\Client();
    }

    public function request(string $method, string $endpoint, array $body)
    {
        try {
            $response = $this->client->request($method, $endpoint, [
                'auth' => [
                    $this->authUsername, $this->authPassword
                ],
                'json' => $body,
            ]);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            echo $e;
            return null;
        }

        return $response->getBody()->getContents();
    }
}
?>