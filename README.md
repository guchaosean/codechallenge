## Description

Simple PHP script for the Eevi code challenge, my resume is also included in this repo.

## Way to run the script

* Clone this repo
* Run ``composer install``
* Run ``php codeChallenge.php``
* It will automatically go thr every step, from step 1 to code submit.